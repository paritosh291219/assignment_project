const express = require("express");
const aes256 = require("aes256");

var router = express.Router();

var { User } = require("../Model/userModel");

var key = "Ah5Su2JWUrIyzBGO7fvFEtblSqCZknQZ";

router.get("/:username", (req, res) => {
  console.log(req.params.username);
  var query = req.params.username;

  User.findOne(
    {
      username: query
    },
    (err, desc) => {
      if (!err) {
        res.send(desc);
        console.log("Fetch Success");
      } else console.log("Fail :" + JSON.stringify(err, undefined, 2));
    }
  );
});

router.post("/register", (req, res) => {
  console.log(req.body.username);
  var query = req.body.username;

  req.body.password = aes256.encrypt(key, req.body.password);

  User.findOne(
    {
      username: query
    },
    (err, doc) => {
      if (err) {
        console.log("Error");
      }
      if (doc) {
        console.log("Username exist");
        return res.status(400).send(`Username Exist`);
      } else {
        var user = new User({
          name: req.body.name,
          address: req.body.address,
          username: req.body.username,
          age: req.body.age,
          organisation: req.body.organisation,
          dob: req.body.dob,
          experience: req.body.experience,
          password: req.body.password
        });

        user.save((err, doc) => {
          if (!err) {
            res.send(doc);
            console.log("Success insert");
          } else console.log("fail :" + JSON.stringify(err, undefined, 2));
        });
      }
    }
  );
});

router.post("/validate", (req, res) => {
  console.log(req.body.username);

  var query = req.body.username;
  var query2 = req.body.password;

  User.findOne(
    {
      username: query
    },
    (err, doc) => {
      if (err) {
        console.log("Error");
      }
      if (doc) {
        console.log("Username exist");

        var decryptedPassword = aes256.decrypt(key, doc.password);

        if (decryptedPassword == query2) {
          console.log("Authentication Success");
          res.send(doc);
        } else return res.status(401).send(`Authentication Failed`);
      } else {
        console.log("Authentication Failed");
        return res.status(401).send(`Authentication Failed`);
      }
    }
  );
});

module.exports = router;
