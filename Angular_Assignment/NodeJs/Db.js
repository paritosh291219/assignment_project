const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/Assignment', (err) => {
    if (!err) {
        console.log('Connection Established');
    } else {
        console.log('Connection Error : ' + JSON.stringify(err, undefined, 2));
    }
});

module.exports = mongoose;