const express = require('express');
const bodyParser = require('body-parser');

const {
    mongoose
} = require('./Db.js');

var userController = require('./Controller/userController');

const app = express();

app.use(bodyParser.json());

app.listen(5000, () => console.log('Express port on 5000'));

app.use('/user', userController);