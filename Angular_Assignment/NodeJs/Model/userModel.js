const mongoose = require('mongoose');

var User = mongoose.model('User', {
    name: {
        type: String
    },
    address: {
        type: String
    },
    username: {
        type: Number
    },
    age: {
        type: Number
    },
    organisation: {
        type: String
    },
    DOB: {
        type: Date
    },
    experience: {
        type: Number
    },

    username: {
        type: String
    },
    password: {
        type: String
    },

});

module.exports = {
    User: User
};