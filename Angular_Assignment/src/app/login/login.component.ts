import { Component, OnInit } from "@angular/core";
import { CommonService } from "../Shared/common.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [CommonService]
})
export class LoginComponent implements OnInit {
  constructor(private commonService: CommonService) {}

  ngOnInit() {}
}
